//
//  HighApp.swift
//  High
//
//  Created by Consultant on 3/15/23.
//

import SwiftUI

@main
struct HighApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: SchoolsViewModel(networkManager: NetworkManager()), isErrorOccured: false)
        }
    }
}
