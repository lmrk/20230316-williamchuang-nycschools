//
//  NetworkManager.swift
//  APICallSwiftUI
//
//  Created by Consultant on 3/15/23.
//

import Foundation

protocol Networkable{
    func getDataFromAPI(url: URL) async throws -> Data
}
final class NetworkManager:Networkable{
    
    func getDataFromAPI(url: URL) async throws -> Data{
        
        do{
            let (data,_)  =  try await URLSession.shared.data(from: url)
            return data
        }catch let error{
            print(error.localizedDescription)
            throw NetworkError.dataNotFound
        }
    }
    
    
}

