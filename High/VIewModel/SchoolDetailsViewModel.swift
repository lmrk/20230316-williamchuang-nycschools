//
//  SchoolDetailsViewModel.swift
//  High
//
//  Created by Consultant on 3/15/23.
//

import Foundation
class SchoolDetailsViewModel:ObservableObject{
    
    @Published var schoolDetail: SchoolSatDetail?
    @Published var customError: NetworkError?
    let networkManager : Networkable

    init( networkManager: Networkable) {
        self.networkManager = networkManager
    }
    
    func getSchoolsSatDetails(urlString:String) async {
        guard let url =  URL(string:urlString) else {
            customError = NetworkError.invalidURL
            return
        }
        do{
            let rawData = try await networkManager.getDataFromAPI(url: url)
           let schoolDetailsParsedData = try JSONDecoder().decode([SchoolSatDetail].self, from: rawData)
            DispatchQueue.main.async {
                self.schoolDetail = schoolDetailsParsedData.first
                print(schoolDetailsParsedData)
            }
            
        }catch let someError{
            print(someError.localizedDescription)
            DispatchQueue.main.async {
                
                if someError as? NetworkError == .dataNotFound{
                    self.customError = NetworkError.dataNotFound
                }else{
                    self.customError = NetworkError.parsingError
                }
            }

        }
        
    }
}
