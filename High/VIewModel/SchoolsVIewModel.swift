//
//  SchoolsVIewModel.swift
//  High
//
//  Created by Consultant on 3/15/23.
//

import Foundation
class SchoolsViewModel:ObservableObject{
    
    @Published var schoolsList: [School] = []
    @Published var customError: NetworkError?
    let networkManager : Networkable

    init( networkManager: Networkable) {
        self.networkManager = networkManager
    }
    
    func getSchoolsList(urlString:String) async {
        guard let url =  URL(string:urlString) else {
            customError = NetworkError.invalidURL
            return
        }
        do{
            let rawData = try await networkManager.getDataFromAPI(url: url)
           let schoolParsedData = try JSONDecoder().decode([School].self, from: rawData)
            DispatchQueue.main.async {
                self.schoolsList = schoolParsedData
            }
            
            print(self.schoolsList)
        }catch let someError{
            print(someError.localizedDescription)
            DispatchQueue.main.async {
                
                if someError as? NetworkError == .dataNotFound{
                    self.customError = NetworkError.dataNotFound
                }else{
                    self.customError = NetworkError.parsingError
                }
            }

        }
        
    }
}
