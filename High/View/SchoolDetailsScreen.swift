//
//  SchoolDetailsScreen.swift
//  High
//
//  Created by Consultant on 3/15/23.
//

import SwiftUI

struct SchoolDetailsScreen: View {
    var schoolData: School
    @StateObject var viewModel : SchoolDetailsViewModel
    @State var isErrorOccured: Bool

    var body: some View {

            ZStack{
                
                LinearGradient(
                    gradient: Gradient(colors: [.white, Color("BlueBack")]),
                    startPoint: .top,
                    endPoint: .bottom)
                .ignoresSafeArea()
                
                VStack {
                    HStack(alignment: .top){
                        Text((schoolData.school_name ?? "")).padding().font(.headline)
                            .font(.title.bold())
                            .foregroundColor(.black)
                        Spacer()
                        Text(schoolData.dbn ?? "") //DBN
                            .font(.title2).bold()
                            .foregroundColor(.red)
                        
                        
                    }.padding()
                    
                    Image(systemName: "building.columns.circle.fill")
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 180, height: 180)
                        .padding(.bottom, 5)
                    
                    
                    Divider()
                    
                    ScrollView {
                        Section(content: {
                            VStack(alignment: .leading, spacing: 7)
                            {
                                Text("Math Avg Score: " + (viewModel.schoolDetail?.satMathAvgScore ?? "") ).font(.body)
                                Text("Reading Avg. Score: " + (viewModel.schoolDetail?.satCriticalReadingAvgScore ?? "") ).font(.body)
                                Text("Writing Avg Score: " + (viewModel.schoolDetail?.satWritingAvgScore ?? "") ).font(.body)
                            }.frame(minWidth: 350, alignment: .leading)
                                .font(.system(size: 18, weight: .medium, design: .rounded))
                                .padding(.bottom, 10)
                        }, header: {
                            Text("SAT scores ").font(.headline)
                        })
                        Divider()
                        Section(content: {
                            VStack(alignment: .leading, spacing: 10)
                            {
                                Text("Overview:")
                                    .fontWeight(.bold)
                                Text(schoolData.overview_paragraph ?? "")
                                    .fontWeight(.bold)
                                Text((schoolData.primary_address_line_1 ?? "") ).padding().font(.headline)
                                Text("Phone Number:")
                                    .fontWeight(.bold)
                                Text(schoolData.phone_number ?? "")
                                Text("School Email:")
                                    .fontWeight(.bold)
                                Text(schoolData.school_email ?? "")
                                
                            }.frame(minWidth: 350, alignment: .leading)
                                .font(.system(size: 18, weight: .medium, design: .rounded))
                                .padding(.bottom, 10)
                        }, header: {
                            Text("Additional info ").font(.headline)
                        })
                        
                        
                    }
                    .padding(5)
                }
            }.task{
                await viewModel.getSchoolsSatDetails(urlString:"\(APIEndPoints.schoolSatsDetails)\(schoolData.dbn!)")
                    if(viewModel.customError != nil){
                        isErrorOccured = true
                    }
            }
            .navigationTitle(Text("Schools Sat Details"))
        }
}

struct SchoolDetailsScreen_Previews: PreviewProvider {
    @State static var school = School(dbn: "", school_name: "", boro: "", overview_paragraph: "", ell_programs: "", neighborhood: "", building_code: "", location: "", phone_number: "", fax_number: "", school_email: "", website: "", subway: "", extracurricular_activities: "", primary_address_line_1: "", city: "")

    static var previews: some View {
        SchoolDetailsScreen(schoolData: school, viewModel: SchoolDetailsViewModel(networkManager: NetworkManager()), isErrorOccured: false)
    }
}
