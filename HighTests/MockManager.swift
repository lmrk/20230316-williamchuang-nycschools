//
//  MockManager.swift
//  HighTests
//
//  Created by Consultant on 3/15/23.
//

import Foundation
@testable import High

class MockManager:Networkable{
    func getDataFromAPI(url: URL) async throws -> Data {
        do{
            let bundle = Bundle(for: MockManager.self)
            guard let path = bundle.url(forResource: url.absoluteString, withExtension: "json") else{
                throw NetworkError.invalidURL
            }
            let data = try Data(contentsOf: path)
            return data
        }catch {
            throw NetworkError.dataNotFound
        }
    }
    
    
}
